package alpine

import (
	"fmt"
	"gopkg.in/ini.v1"
	"os/exec"
	"regexp"
	"strconv"
)

type Service struct {
	Name     string `json:"{#SERVICE.NAME}"`
	Runlevel string `json:"{#SERVICE.RUNLEVEL}"`
}

type ServiceState struct {
	OldState string `json:"name"`
	State    string `json:"state"`
	Runtime  string `json:"runtime"`
	Respawns int    `json:"respawns"`
}

var statePattern = regexp.MustCompile(`(?P<state>\w+)\s*(?:(?P<runtime>\d{2}:\d{2}:\d{2}) \((?P<respawns>\d+)\))?`)

func getOpenRCServices(runlevelFilter string) (services []Service, err error) {
	cmd := exec.Command("/bin/rc-status", "-a", "-f", "ini")
	output, err := cmd.Output()

	if err != nil {
		return []Service{}, err
	}

	iniServices, _err := ini.Load(output)
	if _err != nil {
		err = _err
		return
	}

	for _, runlevel := range iniServices.SectionStrings() {
		if runlevelFilter != "" && runlevel != runlevelFilter {
			continue
		}

		runlevelSection := iniServices.Section(runlevel)

		for _, serviceName := range runlevelSection.KeyStrings() {
			services = append(services, Service{Name: serviceName, Runlevel: runlevel})
		}
	}

	return services, nil
}

func getOpenRCServiceState(service string) (serviceState ServiceState, err error) {
	// TODO: Check that rc-status exists and has the correct version for these flags
	// '-f ini' is supported since 0.41 of openrc
	cmd := exec.Command("/bin/rc-status", "-a", "-f", "ini")
	output, err := cmd.Output()

	if err != nil {
		return ServiceState{}, err
	}

	iniServices, _ := ini.Load(output)
	for _, runlevel := range iniServices.SectionStrings() {
		runlevelSection := iniServices.Section(runlevel)

		for _, serviceName := range runlevelSection.KeyStrings() {
			if serviceName == service {
				rawState := runlevelSection.Key(serviceName).String()
				serviceState = parseOpenRCServiceState(rawState)
				return
			}
		}
	}

	return ServiceState{}, fmt.Errorf("No service with name %s found", service)
}

func parseOpenRCServiceState(rawState string) (serviceState ServiceState) {
	match := statePattern.FindStringSubmatch(rawState)

	serviceState.State = match[1]
	serviceState.OldState = match[1]
	serviceState.Runtime = match[2]

	respawns, err := strconv.Atoi(match[3])

	if err != nil {
		respawns = -1
	}

	serviceState.Respawns = respawns

	return
}
