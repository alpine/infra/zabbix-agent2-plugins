package alpine

import (
	"encoding/json"
	"fmt"

	"git.zabbix.com/ap/plugin-support/log"
	"git.zabbix.com/ap/plugin-support/plugin"
)

type Plugin struct {
	plugin.Base
}

var impl Plugin

func SetLogger(logger log.Logger) {
	impl.Logger = logger
}

func (p *Plugin) Export(key string, params []string, ctr plugin.ContextProvider) (result interface{}, err error) {
	switch key {
	case "openrc.service.discovery":
		var filter string
		if len(params) == 0 {
			filter = ""
		} else {
			filter = params[0]
		}
		services, err := getOpenRCServices(filter)

		if err != nil {
			return nil, err
		}

		result, err := json.Marshal(services)

		if err != nil {
			return nil, fmt.Errorf("Could not marshal json data")
		}

		return string(result), nil
	case "openrc.service.state":
		if len(params) == 0 {
			return nil, fmt.Errorf("Required parameter <service> missing")
		}
		serviceState, err := getOpenRCServiceState(params[0])
		if err != nil {
			return nil, err
		}

		serviceStateJson, err := json.Marshal(serviceState)

		if err != nil {
			return nil, err
		}

		return string(serviceStateJson), nil
	case "alpine.iface.details":
		if len(params) == 0 {
			return nil, fmt.Errorf("Required parameter <name> missing")
		}

		iface, err := GetInterfaceDetails(params[0])

		if err != nil {
			return nil, err
		}

		interfaceJson, err := json.Marshal(iface)

		if err != nil {
			return nil, err
		}

		return string(interfaceJson), nil

	default:
		return nil, plugin.UnsupportedMetricError
	}
}

func init() {
	plugin.RegisterMetrics(&impl, "alpine",
		"openrc.service.discovery", "Returns a JSON array of discovered openrc services, usage: openrc.service.discovery[<runlevel>].",
		"openrc.service.state", "Returns the state of the specified service. usage: openrc.service.state[<service>].",
		"alpine.iface.details", "Returns a JSON object with details about the specific interface. usage: alpine.iface.details[<name>].",
	)
}
