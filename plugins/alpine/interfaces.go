package alpine

import (
	"fmt"
	"path/filepath"
	"syscall"
)

const SYSFS_PATH = "/sys/class/net/"

type IfaceType string

const (
	IFACE_ETHERNET IfaceType = "ethernet"
	IFACE_LOOPBACK IfaceType = "loopback"
	IFACE_PPP      IfaceType = "ppp"
	IFACE_IPIP     IfaceType = "ipip"
	IFACE_IPIP6    IfaceType = "ipip6"
	IFACE_SIT      IfaceType = "sit"
	IFACE_GRE      IfaceType = "gre"
	IFACE_BRIDGE   IfaceType = "bridge"
	IFACE_BOND     IfaceType = "bond"
	IFACE_UNKNOWN  IfaceType = "uknown"
)

type Interface struct {
	Name       string    `json:"name"`
	Type       IfaceType `json:"iface_type"`
	OperState  string    `json:"operState"`
	AdminState string    `json:"adminstate"`
}

type sysfsInterface struct {
	Name string
	Path string
}

var interfaceTypes = map[int]string{
	syscall.ARPHRD_ETHER:    "ethernet",
	syscall.ARPHRD_LOOPBACK: "loopback",
}

func GetInterfaceDetails(name string) (iface Interface, err error) {
	sysIface, _err := makeSysfsInterface(name)

	if _err != nil {
		err = _err
		return
	}

	iface.Name = name
	iface.Type = sysIface.interfaceType()

	operState, _err := sysIface.operState()
	if _err == nil {
		iface.OperState = operState
	} else {
		iface.OperState = "unknown"
	}

	adminState, _err := sysIface.adminState()
	if _err == nil {
		iface.AdminState = adminState
	} else {
		iface.AdminState = "unknown"
	}

	return
}

func makeSysfsInterface(name string) (iface sysfsInterface, err error) {
	ifacePath := filepath.Join(SYSFS_PATH, name)

	if !pathExists(ifacePath) {
		err = fmt.Errorf("Unknown interface %s", name)
		return
	}

	iface.Name = name
	iface.Path = ifacePath

	return

}

func makeInterface(name string) (iface Interface) {
	iface = Interface{
		Name: name,
	}

	return
}

func (iface sysfsInterface) interfaceType() (ifaceType IfaceType) {
	iface_type_val, err := fileReadInt(filepath.Join(iface.Path, "type"))

	if err != nil {
		fmt.Println(err)
		ifaceType = IFACE_UNKNOWN
		return
	}

	switch iface_type_val {
	case syscall.ARPHRD_ETHER:
		ifaceType = IFACE_ETHERNET
		switch {
		case iface.isBridge():
			ifaceType = IFACE_BRIDGE
		case iface.isBond():
			ifaceType = IFACE_BOND
		}
	case syscall.ARPHRD_LOOPBACK:
		ifaceType = IFACE_LOOPBACK
	case syscall.ARPHRD_PPP:
		ifaceType = IFACE_PPP
	case syscall.ARPHRD_IPGRE:
		ifaceType = IFACE_GRE
	default:
		ifaceType = IFACE_UNKNOWN
	}

	return
}

func (iface sysfsInterface) isBridge() bool {
	return pathExists(filepath.Join(iface.Path, "bridge"))
}

func (iface sysfsInterface) isBond() bool {
	return pathExists(filepath.Join(iface.Path, "bonding"))
}

func (iface sysfsInterface) ifIndex() (index int, err error) {
	index, err = fileReadInt(filepath.Join(iface.Path, "ifindex"))
	return
}

func (iface sysfsInterface) ifLink() (link int, err error) {
	link, err = fileReadInt(filepath.Join(iface.Path, "iflink"))
	return
}

func (iface sysfsInterface) operState() (operState string, err error) {
	operState, err = fileReadString(filepath.Join(iface.Path, "operstate"))
	return
}

func (iface sysfsInterface) adminState() (adminState string, err error) {
	flags, _err := fileReadHex(filepath.Join(iface.Path, "flags"))

	if _err != nil {
		err = _err
		return
	}

	switch flags & syscall.IFF_UP {
	case 1:
		adminState = "up"
	default:
		adminState = "down"
	}

	return
}
