package alpine

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func pathExists(path string) bool {
	_, err := os.Lstat(path)

	return err == nil
}

func fileReadString(path string) (content string, err error) {
	contentBytes, err := ioutil.ReadFile(path)

	if err != nil {
		return "", err
	}

	content = strings.TrimSpace(string(contentBytes))
	return
}

func fileReadInt(path string) (val int, err error) {
	content, err := fileReadString(path)

	if err != nil {
		return 0, err
	}

	value, err := strconv.Atoi(content)

	if err != nil {
		return 0, err
	}

	return value, nil
}

func fileReadHex(path string) (val int, err error) {
	content, err := fileReadString(path)

	if err != nil {
		return 0, nil
	}

	value, err := parseFormattedHex(content)

	if err != nil {
		return 0, err
	}

	return value, nil
}

func parseFormattedHex(strVal string) (value int, err error) {
	var valueStripped string
	_, err = fmt.Sscanf(strVal, "0x%s", &valueStripped)

	if err != nil {
		return 0, err
	}

	val, err := strconv.ParseInt(valueStripped, 16, 32)

	if err != nil {
		return 0, err
	}

	return int(val), nil
}
