module gitlab.alpinelinux.org/alpine/infra/zabbix-agent2-plugins

go 1.19

require (
	git.zabbix.com/ap/plugin-support v1.2.2-0.20230530073137-9c948d2bdd68
	gopkg.in/ini.v1 v1.66.6
)

require (
	github.com/Microsoft/go-winio v0.6.0 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/tools v0.1.12 // indirect
)
