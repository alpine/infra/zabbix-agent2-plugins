# Plugins for Zabbix Agent 2

This is a collection plugins for the Zabbix go agent. They are specifically
targeted to Alpine Linux.

## Building

``` go
go build ./cmd/zabbix-agent2-plugin-alpine.go
```

## Installation

Copy the binary to `/usr/libexec/`. Copy the configuration file to
`/etc/zabbix/zabbix_agent2.d/plugins.d/` and restart the agent.

## Supported items

### Interfaces

* `alpine.iface.details[<name>]` - Information about the state of a
  named interface as a json object with the following properties:
    * `name`
    * `iface_type`
    * `operState` - The operational state of the interface (up, down or unknown)
    * `adminstate`- The administrative state of the interface, in other words,
      if the interface is explicitly enabled or disabled. (up, down or unknown)

### OpenRC

* `openrc.service.discovery` - List of known services in LLD format
    * `{#SERVICE.NAME}`
    * `{#SERVICE.RUNLEVEL}`
* `openrc.service.state[<service>]` - The state of a service as a json object
    * `state` - The state of the service (started, stopped, crashed)
    * `runtime` - How long is the service running (only supported for supervised
      services)
    * `respawns` - The amount of times the service has been restarted after a crash

