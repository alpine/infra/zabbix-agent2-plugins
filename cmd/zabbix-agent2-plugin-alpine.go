package main

import (
	"fmt"

	"git.zabbix.com/ap/plugin-support/plugin/container"
	"gitlab.alpinelinux.org/alpine/infra/zabbix-agent2-plugins/plugins/alpine"
)

func main() {
	h, err := container.NewHandler("alpine")

	if err != nil {
		panic(fmt.Sprintf("failed to create plugin handler %s", err.Error()))
	}

	alpine.SetLogger(&h)

	err = h.Execute()
	if err != nil {
		panic(fmt.Sprintf("failed to execute plugin handler %s", err.Error()))
	}
}
